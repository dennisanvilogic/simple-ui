import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import "./index.css";
import App from "./App";
import { MuiThemeProvider } from "@material-ui/core";
import { ConnectedRouter } from "react-router-redux";
import AnviLogicTheme from "./AnviLogicTheme";
import { store, history } from "./store";

import reportWebVitals from "./reportWebVitals";

const MOUNT_NODE = document.getElementById("root");

ReactDOM.render(
  <React.StrictMode>
    <MuiThemeProvider theme={AnviLogicTheme.dark}>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </Provider>
    </MuiThemeProvider>
  </React.StrictMode>,
  MOUNT_NODE
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
