import React, { useEffect, useLayoutEffect, useRef } from "react";
import { Switch, Redirect, Route } from "react-router-dom";
import clsx from "clsx";

import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import logo from "./logo.svg";
import "./App.css";
import Home from "./pages/Home";
import Page1 from "./pages/Page1";
import Page2 from "./pages/Page2";
import { history } from "./store";

function App(props) {
  const ref = useRef();

  return (
    <Box
      className="App"
      style={{
        display: "flex",
        overflowY: "visible",
        backgroundColor: "yellow",
      }}
    >
      <main ref={ref} id="main">
        <Switch>
          <Route exact path={"/"} render={() => <Home history={history} />} />
          <Route
            exact
            path={"/page1"}
            render={() => <Page1 history={history} />}
          />
          <Route
            exact
            path={"/page2"}
            render={() => <Page2 history={history} />}
          />
        </Switch>
      </main>
    </Box>
  );
}

export default App;
