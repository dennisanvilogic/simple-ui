import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { fromJS } from "immutable";
// import { routerMiddleware } from 'connected-react-router'
import { routerMiddleware } from "react-router-redux";
import { saveState } from "./utils/redux-storage";
import createReducer from "./reducers";

export default function configureStore(initialState = {}, history) {
  const middlewares = [routerMiddleware(history), thunk];

  // if (process.env.NODE_ENV !== 'production') {
  /* eslint-disable global-require */
  // const { createLogger } = require('redux-logger');

  // middlewares.push(createLogger({
  //     collapsed: true,
  //     duration: true,
  //     stateTransformer: (state) => Iterable.isIterable(state) ? state.toJS() : state
  // }));
  // }

  const enhancers = [applyMiddleware(...middlewares)];

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers =
    process.env.NODE_ENV !== "production" &&
    typeof window === "object" &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
          // TODO Try to remove when `react-router-redux` is out of beta, LOCATION_CHANGE should not be fired more than once after hot reloading
          // Prevent recomputing reducers for `replaceReducer`
          shouldHotReload: false,
        })
      : compose;
  /* eslint-enable */

  const store = createStore(
    // createReducer(history),
    createReducer(),
    fromJS(initialState),
    composeEnhancers(...enhancers)
  );

  // Extensions
  store.injectedReducers = {}; // Reducer registry
  store.subscribe(() => {
    saveState(store.getState());
  });

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  if (module.hot) {
    module.hot.accept("./reducers", () => {
      store.replaceReducer(createReducer(store.injectedReducers));
    });
  }

  return store;
}
