import { createMuiTheme } from "@material-ui/core";

const fontFamily =
  "Roboto, Segoe UI, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,sans-serif";
// "Roboto Condensed, Segoe UI, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,sans-serif";

const AnvilLogicPrimary = {
  stepColor: "#2779B7",
  main: "#309DDF", //blue
  light: "#34ABEE", //light baby blue
  dark: "#2B8ACB",
  contrastText: "#FFFFFF", //white
  fontFamily,
  // funnelDarkest: "#1F5994",
  funnelDark: "#1F5994",
  funnelMain: "#2B7ACA",
  // funnelLight: "#89B8E6",
  funnelLight: "#5799DB",
  error: "#F44336",
};

const AnvilLogicLightPrimary = {
  stepColor: "#2779B7",
  main: "#00188F", //blue
  light: "#34ABEE", //light baby blue
  dark: "#2B8ACB",
  contrastText: "#FFFFFF", //white
  fontFamily,
  // funnelDarkest: "#1F5994",
  funnelDark: "#1F5994",
  funnelMain: "#2B7ACA",
  // funnelLight: "#89B8E6",
  funnelLight: "#5799DB",
  error: "#F44336",
};
const AnvilLogicSecondary = {
  stepColor: "#045D56",
  main: "#045d56", //green
  dark: "#06423a", // dark green
  light: "#dff0f0", //light turqoise
  contrastText: "#A3A5A6",
  fontFamily,
};
const AnvilLogicLightSecondary = {
  stepColor: "#045D56",
  main: "#045d56", //green
  dark: "#06423a", // dark green
  light: "#dff0f0", //light turqoise
  contrastText: "#A3A5A6",
  fontFamily,
};

// colors for main and light are swtiched because they are inverted in the code
const AnvilLogicSurface = {
  "900": "#0E0E0E", //darkPure, rgb(14,14,14)
  "850": "rgba(255,255,255,0.05)", //darkOpacity
  "800": "#121212", //dark, rgba(18,18,18,1)
  "700": "#1A1A1A", //darkOnDark,  rgba(26,26,26,1)
  "600": "#1E1E1E", // matrix default background, rgba(30,30,30,1)
  "500": "#1F1F1F", // rgb(31, 31, 31), lightPure
  "450": "rgba(255,255,255,0.12)", //lightOpacity
  "400": "#1A1E21", //main, rgba(26,30,33,1)
  "300": "#252525", //medium, not used..?
  "200": "#1F262A",
  "100": "#263238", //light, rgba(37,37,37,1)
  lightMedium: "#515151", //  rgba(81,81,81,1)
  contrastText: "#FFFFFF",
  contrastTextOpacity: "rgba(255, 255, 255, 0.7)", //FUTURE FIX: make surface contrast Text have opacity, while primary has pure white
  fontFamily,
  boxShadow:
    "0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2)",
};
const AnvilLogicLightSurface = {
  // main: "#1A1E21", //use case and rule card backgrounds
  // dark: "#000000", //black background
  // light: "gray", // top and side banner background
  // contrastText: "#FFFFFF",
  // contrastTextOpacity: "rgba(255, 255, 0.8)",
  // fontFamily,
  // darkOpacity: "rgba(255,255,255,0.05)",
  // darkPure: "rgb(14, 14, 14)",
  // lightOpacity: "rgba(255,255,255,0.12)",
  // lightPure: "rgb(31, 31, 31)",
  // boxShadow: "0 2px 4px 0 rgba(255,255,255,0.5)",
  "900": "#0E0E0E", //darkPure, rgb(14,14,14)
  "850": "rgba(255,255,255,0.05)", //darkOpacity
  "800": "#121212", //dark, rgba(18,18,18,1)
  "700": "#1A1A1A", //darkOnDark,  rgba(26,26,26,1)
  "600": "#1E1E1E", // matrix default background, rgba(30,30,30,1)
  "500": "#1F1F1F", // rgb(31, 31, 31), lightPure
  "450": "rgba(255,255,255,0.12)", //lightOpacity
  "400": "#1A1E21", //main, rgba(26,30,33,1)
  "300": "#252525", //medium, not used..?
  "100": "#263238", //light, rgba(37,37,37,1)
  lightMedium: "#515151", //  rgba(81,81,81,1)
  contrastText: "#FFFFFF",
  contrastTextOpacity: "rgba(255, 255, 255, 0.7)", //FUTURE FIX: make surface contrast Text have opacity, while primary has pure white
  fontFamily,
  boxShadow:
    "0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2)",
};
const AnviLogicPriorityColors = {
  high: "rgba(224,32,32,0.4)",
  medium: "rgba(250,100,0,0.4)",
  low: "rgba(247,181,0,0.4)",
  ruby: "#FA0000",
  amber: "#FA6400",
  citrine: "#F7B500",
  border: "#817E7E",
  selectBackground: "#565656",
  hoverBackground: "#393939",
  average: "rgba(129,15,124,0.4)",
  magenta: "#810F7C", //purples for data feeds quality
};

const GaugeChart = {
  poor: "#E02020",
  fair: "#FA6400",
  good: "#2B8AC8",
  strong: "#026D65",
};

const Productivity = {
  Ideal: {
    color: "rgba(2,109,101,0.4)",
    border: "#026D65",
  },
  Current: {
    color: "rgba(43,138,200,0.4)",
    border: "#2B8AC8",
  },
};

const AnviLogicCoverageColors = {
  primaryLight: "#5DC4F3", //blues
  primaryLightOpacity: "rgba(93,196,243,0.4)",
  primaryMain: "#288ABC",
  primaryMainOpacity: "rgba(40,138,203,0.4)",
  primaryDark: "#1F5994",
  primaryDarkOpacity: "rgba(31,89,148,0.4)",
  secondaryLight: "#367D7B", //Greens
  secondaryLightOpacity: "rgba(54, 125, 123, 0.2)",
  secondaryMain: "#026D65",
  secondaryMainOpacity: "rgba(2,109,101,0.4)",
  secondaryDark: "#045D56",
  secondaryDarkOpacity: "rgba(4, 47, 40, 0.4)",
  correlationFill: "#2779B7",
  correlationEmpty: "#AAABAB",
};

const AnvilogicQualityColors = {
  goodOpacity: "rgba(4,93,86,0.4)",
  averageOpacity: "rgba(129,15,124,0.4)",
  poorOpacity: "rgba(250,100,0,0.4)",
  unknown: "#7B8487",
};

const AnviLogicBackground = {
  paper: "#263238",
  default: "rgb(14, 14, 14)",
};
const AnviLogicLightBackground = {
  paper: "#263238",
  default: "#999",
};

const CssBaseLineOverride = {
  "@global": {
    body: {
      fontFamily: fontFamily,
      "@media print": {
        backgroundColor: AnviLogicBackground.default,
      },
    },
  },
};

const AnviLogicTheme = createMuiTheme({
  palette: {
    surface: AnvilLogicSurface,
    primary: AnvilLogicPrimary,
    secondary: AnvilLogicSecondary,
    background: AnviLogicBackground,
    priorities: AnviLogicPriorityColors,
    gauge: GaugeChart,
    productivity: Productivity,
    coverage: AnviLogicCoverageColors,
    quality: AnvilogicQualityColors,
    themeName: "AnviLogic 2020",
    type: "dark",
    fontFamily,
  },
  typography: { fontFamily },
  overrides: {
    MuiCssBaseline: CssBaseLineOverride,
  },
});

const AnviLogicLightTheme = createMuiTheme({
  palette: {
    surface: AnvilLogicLightSurface,
    primary: AnvilLogicLightPrimary,
    secondary: AnvilLogicLightSecondary,
    background: AnviLogicLightBackground,
    priorities: AnviLogicPriorityColors,
    gauge: GaugeChart,
    productivity: Productivity,
    coverage: AnviLogicCoverageColors,
    quality: AnvilogicQualityColors,
    themeName: "AnviLogic 2020",
    type: "light",
    fontFamily,
  },
  typography: { fontFamily },
  overrides: {
    MuiCssBaseline: CssBaseLineOverride,
  },
});

const ThemeObject = {
  dark: AnviLogicTheme,
  light: AnviLogicLightTheme,
};

export default ThemeObject;
