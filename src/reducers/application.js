import { fromJS } from "immutable";
import ActionTypes from "../actions/types";

import { USE_CASES } from "../utils/constants";

export const initialState = fromJS({
  isLoading: false,
  isLoggedIn: false,
  error: null,
  user: null,
  currentSignupEmail: null,
  comingSoon: {
    show: false,
    readableComponentString: null,
  },
  snackBar: {
    show: false,
    message: null,
  },
  serverError: null,
  searchDocType: USE_CASES,
  searchQuery: "",
  isSearchQuerySet: true,
  siemConfigsList: {},
  threatScenario: null,
  // mitre: null,
  allNotifications: null,
  // State to let you know whether or not to put page in edit mode, or move page to rule page. Will be removed when
  // Rule/Usecase page is merged
  TS_usecases: [],
});

export default function application(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.TS_USECASES_SET:
      return state.merge({
        TS_usecases: action.payload,
      });
    case ActionTypes.SEARCH_QUERY_SET: {
      return state.merge({
        searchQuery: action.payload,
        isSearchQuerySet: false,
      });
    }
    case ActionTypes.SEARCH_QUERY_SET_DONE: {
      return state.merge({ isSearchQuerySet: true });
    }
    case ActionTypes.SEARCH_TYPE_SELECTED: {
      return state.merge({ searchDocType: action.payload });
    }
    case ActionTypes.COMING_SOON_SHOW:
      return state.merge({
        comingSoon: {
          show: true,
          readableComponentString: action.payload,
        },
      });
    case ActionTypes.COMING_SOON_HIDE:
      return state.merge({
        comingSoon: {
          show: false,
          readableComponentString: null,
        },
      });
    case ActionTypes.SNACK_BAR_SHOW:
      return state.merge({
        snackBar: {
          show: true,
          message: action.payload.message,
          variant: action.payload.variant ? action.payload.variant : "success",
        },
      });
    case ActionTypes.SNACK_BAR_HIDE:
      return state.merge({
        snackBar: {
          show: false,
          message: null,
          variant: null,
        },
      });
    case ActionTypes.SIEM_CONFIGS_SET: {
      return state.merge({ siemConfigsList: action.payload });
    }
    case ActionTypes.INIT_EDIT_THREAT_SCENARIO: {
      return state.merge({ threatScenario: action.payload });
    }
    case ActionTypes.STARTED_EDIT_THREAT_SCENARIO: {
      return state.merge({ threatScenario: null });
    }
    case ActionTypes.LOGIN_SUCCESS:
      return state.merge({
        isLoggedIn: true, // will need to change this when MFA comes.
        isLoading: false,
        user: action.payload.userInfo,
      });
    case ActionTypes.REGISTRATION_ERROR:
      return state.merge({ isLoading: false, error: action.error });
    case ActionTypes.LOGOUT:
      return state.merge({
        isLoading: false,
        user: null,
        isLoggedIn: false,
        searchDocType: USE_CASES,
      });
    case ActionTypes.LOGIN_REQUEST:
      return state.merge({ isLoading: true, error: null });
    case ActionTypes.LOGIN_ERROR:
      return state.merge({ isLoading: false, error: action.error });
    case ActionTypes.RESET_ERROR:
      return state.merge({ error: null });
    case ActionTypes.SERVER_ERROR_RESET:
      return state.merge({ isLoading: false, serverError: null });
    case ActionTypes.SERVER_ERROR:
      return state.merge({ isLoading: false, serverError: action.error });
    case ActionTypes.EXPIRED_SESSION:
      return state.merge({ isLoading: false, error: action.error });
    case ActionTypes.SET_ALL_NOTIFICATIONS:
      return state.merge({ allNotifications: action.payload });
    case ActionTypes.SET_FULL_VERSION_INFO:
      return state.merge({ fullVersionInfo: action.payload });
    default:
      return state;
  }
}
