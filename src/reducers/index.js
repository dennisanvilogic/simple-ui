import { combineReducers } from "redux-immutable";
import { fromJS } from "immutable";
import application from "./application";
// import { connectRouter, LOCATION_CHANGE } from "connected-react-router/immutable";
import { LOCATION_CHANGE } from "react-router-redux";

const routeInitialState = fromJS({
  location: null,
});

function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return state.merge({
        location: action.payload,
      });
    default:
      return state;
  }
}

export default function createReducer(injectedReducers) {
  return combineReducers({
    application,
    route: routeReducer,
    ...injectedReducers,
  });
}
