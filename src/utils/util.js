import _ from "lodash";
import moment from "moment";

// import MitreUtil from "../containers/AnvilMitrePage/MitreUtil";
import { RECAPTCHA_SITE_KEY } from "./constants";

const resolveAPIURL = () => {
  if (window.location.href.startsWith("http://localhost:3000")) {
    // return process.env.API_URL;
    // return "https://52.72.140.67/api/"; // Benji AWS
    // return "https://18.206.193.8/api/"; // Sathya AWS
    // return "https://10.1.10.191/api/"; // Sathya local
    // return "https://10.1.10.102/api/"; // Sathya local2
    // return "https://10.1.10.98/api/"; // Shekar
    // return "https://10.1.10.176/api/"; // James
    // return "https://10.1.10.103/api/"; // Shekar
    // return "https://10.1.10.195/api/"; // Shekar
    // return "https://10.1.10.149/api/"; // Shekar
    return "https://10.1.10.251/api/"; // Shekar
    // return "https://10.1.10.98/api/"; //Shekar
    // return "https://10.1.10.39/api/"; // Satheesh
    // return "https://52.72.140.67/api/"; // External box
    // return 'https://35.175.108.245/api/';
    // return "https://35.175.108.245/api/";
    // return "https://demo.anvilogic.com/api/";
    // return "https://secure.anvilogic.com/api/"; // Secure
  }
  return `${window.location.origin}/api/`;
};

export const API_URL = resolveAPIURL();

export const getReadableDate = (
  dateInt,
  daysThreshold = 7,
  momentDateFormat = "lll"
) => {
  dateInt =
    typeof dateInt.getTime === "function"
      ? dateInt.getTime()
      : parseInt(dateInt);
  const timeThreshold = moment().subtract(daysThreshold, "days");
  const time = moment(dateInt).isAfter(timeThreshold)
    ? moment(dateInt).fromNow()
    : moment(dateInt).format(momentDateFormat);
  return time;
};

export const arrayToStringFormatted = (
  arr,
  delimiter = ", ",
  dedup = true,
  titleCase = false
) => {
  arr = _.flattenDeep(arr);
  if (dedup) {
    arr = _.uniq(arr);
  }

  const len = arr.length;
  let ret = titleCase ? toTitleCase(arr[0]) : arr[0];
  for (let i = 1; i < len; i++) {
    ret += `${delimiter}${titleCase ? toTitleCase(arr[i]) : arr[i]}`;
  }
  return ret;
};

export const arrayToStringWithRenderer = (arr, rendererFunc) => {
  const formattedArray = [arrayToStringFormatted(arr)];
  return formattedArray.map((element) => rendererFunc(element));
};

export const formatProducitivityPayloadDate = (dateString, type) => {
  if (!(dateString && dateString.split)) {
    return dateString;
  }
  return dateString;
};

export const ellipsizeString = (origStr, maxLengthOfString = 120) => {
  if (!origStr) {
    return origStr;
  }
  if (origStr.length <= maxLengthOfString) {
    return origStr;
  }
  return `${origStr.substr(0, maxLengthOfString - 5)}...`;
};
export const numberWithCommas = (x) => {
  if (x && x.toString) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  return x;
};

export const hasLowerCase = (str) => /[a-z]/.test(str);
export const hasUppercaseCase = (str) => /[A-Z]/.test(str);
export const hasDigits = (str) => /[0-9]/.test(str);
export const hasSpecialChars = (str) => /\W+/.test(str);
export const hasPasswordMinChars = (str) => str.length >= 8;
export const hasMatchingPassword = (password, confirmPassword) =>
  password !== "" ? password === confirmPassword : false;
export const isLegitEmailAddress = (str) =>
  /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(str);

export const isPasswordStrengthAcceptable = (password, confirmPassword) =>
  hasPasswordMinChars(password) &&
  hasUppercaseCase(password) &&
  hasLowerCase(password) &&
  hasDigits(password) &&
  hasSpecialChars(password) &&
  hasMatchingPassword(password, confirmPassword);

export const updateMultiSelectsMap = (previous, nextValue, setter, key) => {
  const newObj = { ...previous };
  newObj[key] = nextValue;
  setter(newObj);
};

export const getTooltipForSubType = (subType) => {
  let returnVal = "";
  switch (subType) {
    case "BEHAVIORAL":
      returnVal =
        "Potentially malicious activity which requires additional correlation";
      break;
    case "SIGNATURE":
      returnVal = "Confirmed/Known malicious indicators";
      break;
    case "THREAT SCENARIO":
      returnVal = "Correlation of one or more threat identifiers";
      break;
    case "THREAT IDENTIFIER":
      returnVal = "Atomic detections for specific threat behavior";
      break;
    case "BASELINE":
      returnVal = "Anomalous activity based on historical patterns";
      break;
    case "MACHINE LEARNING":
      returnVal = "ML code to determine malicious activity";
      break;
    case "SEQUENTIAL CORRELATION":
      returnVal = "Time sequenced correlation on one or more events";
      break;
    case "RISK THRESHOLD":
      returnVal =
        "Deviations from pre-defined risk threshold for a given entity";
      break;
    case "ADVERSARY RECOGNITION":
      returnVal = "Patterns of threat actors";
      break;
    case "None Selected":
      returnVal = "None selected";
      break;
    default:
      returnVal = "Unknown";
  }
  return returnVal;
};

// Largest Remainder Method
export const roundPercentage = (numbers, desiredTotal) => {
  const _upperSum = numbers.reduce((a, b) => a + b);
  const _sortedArray = numbers
    .map((number, index) => {
      const _value = (number / _upperSum) * desiredTotal;
      return {
        floor: Math.floor(_value),
        remainder: Math.round((_value - Math.floor(_value)) * 10000) / 10000,
        index: index,
      };
    })
    .sort((a, b) => b.remainder - a.remainder);

  const _lowerSum = _sortedArray.reduce((a, b) => a + b.floor, 0);
  for (let i = 0; i < desiredTotal - _lowerSum; i++) {
    _sortedArray[i].floor++;
  }

  return _sortedArray.sort((a, b) => a.index - b.index).map((e) => e.floor);
};

let priority_weight = { Unprioritized: 1, Low: 2, Medium: 3, High: 4 };
export const prioritySort = (a, b) => {
  if (priority_weight[a.priority] < priority_weight[b.priority]) {
    return -1;
  } else if (priority_weight[a.priority] === priority_weight[b.priority]) {
    return 0;
  } else return 1;
};

let feed_quality_weight = {
  Good: 6,
  Average: 5,
  Poor: 4,
  Missing: 3,
  UseDefault: 2,
  Unknown: 1,
};
export const feedQualitySort = (a, b) => {
  if (feed_quality_weight[a.feedQuality] < feed_quality_weight[b.feedQuality]) {
    return -1;
  } else if (
    feed_quality_weight[a.feedQuality] === feed_quality_weight[b.feedQuality]
  ) {
    return 0;
  } else return 1;
};
export const scrollToFocus = (elementId, offset) => {
  var elementPosition = document.getElementById(elementId).offsetTop;
  window.scrollTo({ top: elementPosition - offset, behavior: "smooth" });
};

export const handleCsvExport = (csvFormat, data, fileTitle) => {
  let csvContent =
    "data:text/csv;charset=utf-8," +
    csvFormat
      .map((obj) => {
        return obj.title;
      })
      .join(",") +
    "\n" +
    data
      .map((rowObj) => {
        let str = csvFormat
          .map((csvObj) => {
            let val = _.get(rowObj, csvObj.field, "");
            if (Array.isArray(val)) val = `"${val}"`; // wrap val in parenthesis if it is an array
            return val;
          })
          .join(",");
        return str;
      })
      .join("\n");

  var encodeUri = encodeURI(csvContent);
  var link = document.createElement("a");
  link.setAttribute("href", encodeUri);
  link.setAttribute("download", fileTitle);
  document.body.appendChild(link);
  link.click();
};

export const getCsvFormat = (columns) => {
  let csvFormat = [];

  columns.forEach((column) => {
    csvFormat.push({
      title: column.title.props.children.replaceAll("#", ""),
      field: column.field,
    });
  });

  return csvFormat;
};

export const createCsvDataTable = (datas, csvFormat, custom) => {
  let flattenedList = [];
  datas.forEach((data) => {
    let tempObj = {};
    csvFormat.forEach((item) => {
      let attribute;
      if (custom && custom[item.field]) {
        const fnc = custom[item.field].fnc;
        attribute = fnc(data);
      } else {
        attribute = data[item.field] || "";
      }

      if (_.isString(attribute) && attribute.includes("\n")) {
        attribute = attribute.replaceAll("\n", "");
      }

      if (
        _.isString(attribute) &&
        (attribute.includes('"') ||
          attribute.includes("'") ||
          attribute.includes(","))
      ) {
        attribute = attribute.replaceAll('"', '""');
        attribute = '"' + attribute + '"';
      }

      tempObj[item.field] = attribute;
    });
    flattenedList.push(tempObj);
  });
  return flattenedList;
};

export const getNumberAbbreviation = (num, fixed = 1) => {
  if (typeof num === "string") {
    num = Number(num);
  }
  if (num === 0) {
    return 0;
  }
  const suffixes = ["", "K", "M", "B", "T"];
  let numExponentArray = num.toPrecision(2).split("e");
  let k =
    numExponentArray.length === 1
      ? 0
      : Math.floor(Math.min(numExponentArray[1].slice(1), 14) / 3);
  let rawOutput =
    k < 1 ? num.toFixed() + ".0" : (num / Math.pow(10, k * 3)).toFixed(fixed);
  let digitAfterPeriod = rawOutput.split(".")[1][0];
  return digitAfterPeriod === "0"
    ? parseInt(rawOutput) + suffixes[k]
    : rawOutput + suffixes[k];
};

// naive sort.. not optimized. Its here only to avoid bringing in a
// SortedSet library. To be called only where the sorting is required
// occassionally and on a small set.
export const sortSet = (set) => {
  const entries = [];
  // eslint-disable-next-line no-unused-vars
  for (const member of set) {
    entries.push(member);
  }
  set.clear();
  // eslint-disable-next-line no-unused-vars
  for (const entry of entries.sort()) {
    set.add(entry);
  }
};

/* Filter out props to fix console warnings:
    "React does not recognize the prop on a DOM element." */
export const filterOutProps = (props, propsToFilterOut) => {
  const filteredPropsArray = Object.entries(props).filter(([key, value]) => {
    for (let i = 0; i < propsToFilterOut.length; i++) {
      if (key === propsToFilterOut[i]) return false;
    }
    return true;
  });
  return Object.fromEntries(filteredPropsArray);
};

export const analyticsSkipList = [
  "anvilogic.com",
  "ctdefense.com",
  "indiumsoft.com",
];
/* Site key for ReCaptcha v2 */
export const getReCaptchaSiteKeyV2 = () => {
  const hostname = window.location.hostname;
  if (
    hostname.endsWith("anvilogic.com") ||
    hostname.endsWith("anvilogic.net")
  ) {
    return {
      key: RECAPTCHA_SITE_KEY.V2_PROD,
      //hostname: hostname, // don't send in prod: since its visible in Network
    };
  }
  return {
    key: RECAPTCHA_SITE_KEY.V2_TEST,
    hostname: hostname, // backend looks at this to use test SITE_KEY
  };
};

// returns true if event was tracked, otherwise returns false
export const segment = (msg, obj = {}) => {
  try {
    if (window.analytics) {
      const reduxState = JSON.parse(localStorage.getItem("redux-state"));
      const user = reduxState.application.user;
      if (user) {
        if (user && user.email) {
          for (let i = 0; i < analyticsSkipList.length; i++) {
            if (user.email.includes(analyticsSkipList[i])) {
              return;
            }
          }
        }
      }
      window.analytics.track(msg, obj);
      return true;
    }
  } catch (e) {
    console.log(e);
  }
  return false;
};
