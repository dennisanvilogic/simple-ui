const ALL_UPPERCASE_LIST = /^(dll|sid|os|llmnr|nbt|ns|smb)$/i;
/* To Title Case © 2018 David Gouch | https://github.com/gouch/to-title-case */
export const toTitleCase = (str) => {
  // added from to the list
  var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|v.?|vs.?|via|from|opened)$/i;
  // Made period part of the alphanumeric pattern to handle .bashrc and .bash_profile. small words only applies
  // to words not at the beginning or end
  // eslint-disable-next-line no-useless-escape
  var alphanumericPattern = /([\.A-Za-z0-9\u00C0-\u00FF])/;
  //var wordSeparators = /([ :–—-])/;
  var wordSeparators = /([/ :–—-])/; //title cases char after slash

  return str
    .split(wordSeparators)
    .map(function (current, index, array) {
      if (
        /* Check for small words */
        current.search(smallWords) > -1 &&
        /* Skip first and last word */
        index !== 0 &&
        index !== array.length - 1 &&
        /* Ignore title end and subtitle start */
        array[index - 3] !== ":" &&
        array[index + 1] !== ":" &&
        /* Ignore small words that start a hyphenated phrase */
        (array[index + 1] !== "-" ||
          (array[index - 1] === "-" && array[index + 1] === "-"))
      ) {
        return current.toLowerCase();
      }

      /* Ignore intentional capitalization */
      if (current.substr(1).search(/[A-Z]|\../) > -1) {
        return current;
      }

      if (current.search(ALL_UPPERCASE_LIST) > -1) {
        return current.toUpperCase();
      }

      /* Ignore URLs */
      // if (array[index + 1] === ':' && array[index + 2] !== '') {
      //   return current
      // }

      /* Capitalize the first letter */
      return current.replace(alphanumericPattern, function (match) {
        return match.toUpperCase();
      });
    })
    .join("");
};
