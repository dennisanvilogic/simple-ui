const STATE_KEY = "redux-state";
export const saveState = state => {
  const serializedState = JSON.stringify(state);
  try {
    window.localStorage.setItem(STATE_KEY, serializedState);
  } catch (e) {
    console.error(e);
  }
};

export const getState = () => {
  try {
    const state = window.localStorage.getItem(STATE_KEY);
    return JSON.parse(state);
  } catch (e) {
    return null;
  }
};
