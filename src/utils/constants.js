export const LOGIN_PAGE = "/login";
export const DEFAULT_PAGE = "/";
export const INVITES_PAGE = "/manage_invites";
export const HOME_PAGE = "/home";
export const SEARCH_RESULTS_PAGE = "/search_results";
export const SET_PASSWORD_PAGE = "/register";
export const TRUSTED_GROUPS_PAGE = "/trusted_groups";
export const UNAUTHORIZED_PAGE = "/unauthorized";
export const ADMIN_SETTINGS_PAGE = "/account/details";
export const RESET_PW_PAGE = "/reset_password";
export const CHANGE_PASSWORD_PAGE = "/change_password";
export const USE_CASE_DETAILS_PAGE = "/use_case";
export const RULE_DETAILS_PAGE = "/rule";
export const CREATE_NEW_OBJECT_PAGE = "/stepper";
export const ORG_WORKSPACE_PAGE = "/workspace";
export const NOTIFICATIONS_PAGE = "/notifications_page";
export const THREAT_PRIORTIES_PAGE = "/threat_priorities";
export const AUDIT_PAGE = "/audit";
export const USE_CASE_MITRE_PAGE = "/search_results/mitre_att&ck";
export const DOWNLOAD_SPLUNK_APP = "/downloads/splunkApp/latest";
export const IMPORT_RULES_PAGE = "/import_rules";
export const THREAT_SCENARIO_BUILDER_PAGE = "/threat_scenario_builder";
export const FORGOT_PW_PAGE = "/forgot_password";
export const DATA_FEEDS_PAGE = "/data_feeds";
export const MATURITY_SCORE_PAGE = "/maturity_score";
export const MATURITY_SCORE_HISTORY_PAGE = "/maturity_score/history";
export const MATURITY_SCORE_FEED_PAGE = "/maturity_score/feed";
export const MATURITY_SCORE_DETECTION_PAGE = "/maturity_score/detection";
export const MATURITY_SCORE_PRODUCTIVITY_PAGE =
  "/maturity_score/productivity/volume";
export const ACCESS_CONTROL_PAGE = "/access_control";
export const MFA_PAGE = "/mfa";
export const MFA_DUO_HANDLER_PAGE = "/mfa/duo";
// TODO: rename new macro pages to spec
export const MACROS_PAGE = "/macros";
export const MACRO_DETAILS_PAGE = "/macro";
export const ARMORY_PAGE = "/armory";

export const drawerWidth = 240;

export const MS_IN_SEC = 1000;
export const MS_IN_MIN = MS_IN_SEC * 60;
export const MS_IN_HOUR = MS_IN_MIN * 60;
export const MS_IN_DAY = MS_IN_HOUR * 24;

export const USE_CASES = 0;
export const RULES = 1;
export const MACROS = 2;

// export const SELECT_USE_CASE_TYPE = 0;
export const DEFINE_USE_CASE = 0;
export const ADD_USE_CASE_DETAILS = 1;
export const DEFINE_RULE = 2;
export const ADD_RULE_DETAILS = 3;
export const LIST_TRIAGE_STEPS = 4;

export const SAVE_IN_DRAFT = 0;
// export const SAVE_AND_TEST = 1;
// export const SAVE_AND_PUBLISH = 2;
export const SAVE_AND_PUBLISH = 1;

export const ADD_ROLE_PERMISSION = "add_role";
export const ADD_TASK_COMMENT_PERMISSION = "add_task_comment";
export const ADD_TO_WORKSPACE_PERMISSION = "add_to_workspace";
export const ALL_PRIVILEGES_PERMISSION = "all_privileges";
export const CHANGE_ASSIGNEE_PERMISSION = "change_assignee";
export const CHANGE_TASK_STATUS_PERMISSION = "change_task_status";
export const CREATE_NEW_PERMISSION = "create_new";
export const DELETE_CONTENT_PERMISSION = "delete_content";
export const DELETE_TASK_PERMISSION = "delete_task";
export const DELETE_API_KEY_PERMISSION = "delete_api_key";
export const DEPLOY_RULE_PERMISSION = "deploy_rule";
export const DOWNLOAD_DATA_FEEDS_PERMISSION = "download_data_feeds";
export const DOWNLOAD_SPLUNK_APP_PERMISSION = "download_splunk_app";
export const EDIT_DATA_FEEDS_PERMISSION = "edit_data_feeds";
export const EDIT_MY_RULE_PERMISSION = "edit_my_rule";
export const EDIT_MY_USE_CASES_PERMISSION = "edit_my_use_cases";
export const EDIT_RULE_PERMISSION = "edit_rule";
export const EDIT_SHARE_SETTINGS_PERMISSION = "edit_share_settings";
export const EDIT_SIEM_CONFIGURATION_PERMISSION = "edit_siem_configuration";
export const EDIT_THREAT_PRIORITIES_PERMISSION = "edit_threat_priorities";
export const EDIT_TRUSTED_GROUPS_PERMISSION = "edit_trusted_groups";
export const EDIT_USE_CASE_PERMISSION = "edit_use_cases";
export const GENERATE_API_KEY_PERMISSION = "generate_api_key";
export const RATE_RULE_PERMISSION = "rate_rule";
export const SHARE_RULE_PERMISSION = "share_rule";
export const SHARE_USE_CASE_PERMISSION = "share_use_case";
export const SUPER_PRIVILEGES_PERMISSION = "super_privileges";
export const UPLOAD_DATA_FEEDS_PERMISSION = "upload_data_feeds";
export const UNKOWN_PERMISSION = "unknownpermission";
export const VIEW_ADMIN_SETTINGS_PERMISSION = "view_admin_settings";
export const VIEW_AUDIT_EVENTS_PERMISSION = "view_audit_events";
export const VIEW_DATA_FEEDS_PERMISSION = "view_data_feeds";
export const VIEW_MATURITY_SCORE_PERMISSION = "view_maturity_score";
export const VIEW_MY_WORKSPACE_PERMISSION = "view_my_workspace";
export const VIEW_ORG_WORKSPACE_PERMISSION = "view_org_workspace";
export const VIEW_ROLES_PERMISSION = "view_roles";
export const VIEW_RULE_PERMISSION = "view_rule"; // Used for Rule page and Test button on Rules page
export const VIEW_RULE_LIST_PERMISSION = "view_rule_list";
export const VIEW_RULE_LOGIC_PERMISSION = "view_rule_logic";
export const VIEW_SIEM_CONFIGURATION_PERMISSION = "view_siem_configuration";
export const VIEW_TASK_DETAILS_PERMISSION = "view_task_details";
export const VIEW_THREAT_PRIORITIES_PERMISSION = "view_threat_priorities";
export const VIEW_TRUSTED_GROUPS_PERMISSION = "view_trusted_groups";
export const VIEW_USERS_PERMISSION = "view_users";
export const VIEW_USE_CASE_LIST_PERMISSION = "view_use_case_list";
export const VIEW_USE_CASE_PERMISSION = "view_use_case";

export const UserAccessToken = "userAccessToken"; // AP-3081
export const CURRENT_USER = "currentUser"; // AP-4225 - update to AP-3081
export const StorageEvent = "storage";

export const PRODUCTIVITY_SCORE = {
  new: "New",
  false_positives: "False Positives",
  tuning: "Tune/Update",
  escalated: "Escalated",
  other: "Other",
  none: "none",
};
export const PRODUCTIVITY_PAGE_STATE_PARAMS = {
  RANGE: "rangeInDays",
  SELECTED_DAY: "selectedDay",
  SELECTED_PERIOD: "selectedPeriod",
  SELECTED_SEGMENT: "selectedSegment",
};
export const PRODUCTIVITY_INSIGHTS = {
  VOLUME: "Volume",
  TRIAGE_DWELL_TIME: "Triage Dwell Time",
  ALERT_TO_RATIO: "Alert to Analyst Ratio",
  TRIAGE_PCT: "Triage Percentage",
  TRIAGE_INTEGRITY: "Triage Integrity",
  HUNTING: "Hunting",
  AVG_DWELL_TIME: "Average Dwell Time",
};

export const MS_HISTORY_PAGE_STATE_PARAMS = {
  START_DATE: "startDate",
  END_DATE: "endDate",
  SELECTED_DAY: "selectedDay",
};

export const AUDIT_ACTIONS = {
  //   missingAction: "Missing Action",
  registered: "Registered",
  login: "Login",
  logout: "Logout",
  search: "Search",
  viewRule: "View Rule",
  // viewRuleDetail = ; // not required since its a single UI action

  createOrg: "Create Org",
  createUser: "Create User",
  passwordReset: "Password Reset",
  passwordChanged: "Password Changed",
  createUseCase: "Create Use Case",
  deleteUseCase: "Delete Use Case",
  createRule: "Create Rule",
  deleteRule: "Delete Rule",
  editRule: "Edit Rule",
  viewUseCase: "View Use Case",
  createApiKey: "Create API Key",
  deleteApiKey: "Delete API Key",
  manageUser: "Manage User",
  maintenance: "Maintenance", // routine maintenance tasks
  editUseCase: "Edit Use Case", // routine maintenance tasks

  // trusted group stuff
  createTrustedGroup: "Create Trusted Group",
  deleteTrustedGroup: "Delete Trusted Group",

  joinTrustedGroup: "Join Trusted Group",
  leaveTrustedGroup: "Leave Trusted Group",
  declineTrustedGroupInvitation: "Decline Trusted Group Invitation",

  removeOrgFromGroup: "Remove Org From Group",
  // rejectOrgRequest = ;  // reject unsolicited request
  // rejectOrgProposal =; // reject proposal

  addUseCaseToWorkspace: "Add Use Case to Workspace",
  addRuleToWorkspace: "Add Rule to Workspace",

  createdSiemConfig: "Created Siem Config",
  ruleToDeploy: "Deploy Rule", // marked to_deploy
  ruleDeployed: "Rule Deployed", // from App: deploy succeeded
  ruleDeployFailed: "Rule Deploy Failed", // from App: deploy failed
  ruleShared: "Rule Shared",

  download: "Download", // download Splunk App ..
  importFile: "Import File", // imported rule/useCase
  ruleDeployCancelled: "Rule Deploy Cancelled", // from UI: cancelled rule deployment
  useCaseShared: "Use Case Shared",

  addRole: "Added Role",
  updateRole: "Updated Role",
  deleteRole: "Deleted Role",
  setUserStatus: "Changed User Status",
  importFromApp: "Added content from App",
  taskRecoSnoozed: "Task Recommendation Snoozed",
  taskRecoDiscarded: "Task Recommendation Discarded",

  createMacro: "Create Macro",
  editMacro: "Edit Macro",
  deleteMacro: "Delete Macro",
  macroShared: "Macro Shared",
  macroToDeploy: "Deploy Macro",
  macroDeployCancelled: "Macro Deploy Cancelled",
  macroDeployFailed: "Macro Deploy Failed",
  macroDeployed: "Macro Deployed",
};

export const MATURITY_SCORE_ACTIONS = {
  ScoreType_Unknown: "Unknown",
  InitialComputation: "Initial Computation",
  RuleDeployed: "Deployed rules",
  PriorityChanged: "Updated priorities",
  DataFeedChanged: "Updated data feeds",
  AppAnalyticsSync: "Analytics updated from app",
  RuleNotRunning: "Rule is not running",
  TIDeployed: "Deployed threat identifier",
  TSDeployed: "Deployed threat scenario",
  PlatformPrioChanged: "Updated platform priority",
  TGPrioChanged: "Updated threat group priority",
  TechPrioChanged: "Updated MITRE technique priority",
  DcPrioChanged: "Updated data category priority",
  DataFeedAdded: "Added new data feed",
  TINotRunning: "Threat identifier not running",
  TSNotRunning: "Threat scenario not running",
  MitreMappingChanged: "Modified MITRE mapping of a use case",
  DataCategoriesChanged: "Modified data categories of a rule",
  RecomputeNoChange: "System maintenance recomputation job",
};

export const COMMIT_CATEGORIES = [
  "Performance Tuning",
  "Incorrect Logic",
  "Noise Reduction",
  "Other",
];

export const TDT_MAX_MINUTES = 100;
export const AAR_MAX_RATIO = 200;

export const RECAPTCHA_SITE_KEY = {
  V2_TEST: "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI",
  V2_PROD: "6LeHSREcAAAAAKnpFjVYUXXNrYNLEKCMYWwEal1W",
};

export const searchTypeDict = {
  [USE_CASES]: "usecase",
  [RULES]: "rule",
  [MACROS]: "macro",
};

export const MacroTypeList = [
  { label: "Data Normalization", value: "data_retrieval" },
  { label: "Feed Retrieval", value: "feed_normalization" },
  { label: "Rule Enrichment", value: "rule_enrichment" },
  // { label: "Global Filtering", value: "globalFiltering" },
  { label: "Data Storage", value: "data_storage" },
  { label: "Add Fields", value: "add_fields" },
  { label: "Set Conditions", value: "set_conditions" },
  { label: "Fail-Safe", value: "fail_safe" },
];

export const MacroTypeDict = MacroTypeList.reduce((map, elem) => {
  map[elem.value] = elem.label;
  return map;
}, {});

export const SharingLevelsDict = {
  none: "Private",
  trustedgroup: "Trusted Group",
  all: "Public",
};

export const MacroTypes = MacroTypeList.map((elem) => elem.label);

export const UseCaseSubTypes = [
  // -- Threat Identifier Sub Types --
  "Signature",
  "Behavioral",
  "Baseline",
  "Machine Learning",
  // -- Threat Scenario Sub Types --
  "Sequential Correlation",
  "Risk Threshold",
  "Adversary Recognition",
];
