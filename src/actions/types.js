const ActionTypes = {};

const actions = ["LOGIN", "REGISTRATION"];

const singleActions = [
  "LOGOUT",
  "RESET_ERROR",
  "EXPIRED_SESSION",
  "INIT_EDIT_THREAT_SCENARIO",
  "STARTED_EDIT_THREAT_SCENARIO",
  "COMING_SOON_SHOW",
  "COMING_SOON_HIDE",
  "SNACK_BAR_SHOW",
  "SNACK_BAR_HIDE",
  "SERVER_ERROR_RESET",
  "SERVER_ERROR",
  "SEARCH_TYPE_SELECTED",
  "SEARCH_QUERY_SET",
  "SEARCH_QUERY_SET_DONE",
  // "SET_MITRE_UC_MATCHES",
  "SET_ALL_NOTIFICATIONS",
  "SET_FULL_VERSION_INFO",
  "SIEM_CONFIGS_SET",
  "TS_USECASES_SET",
];

const stages = ["_REQUEST", "_SUCCESS", "_ERROR"];

actions.forEach((action) => {
  ActionTypes[action] = action;

  stages.forEach((stage) => {
    const actionType = `${action}${stage}`;
    ActionTypes[actionType] = actionType;
  });
});

singleActions.forEach((action) => {
  ActionTypes[action] = action;
});

export default ActionTypes;
