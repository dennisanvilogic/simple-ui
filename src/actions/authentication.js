import { setUserAccessToken, setRefreshAuthTokenTimer } from "../services";
import SIEMService from "../services/SIEMService";
import permissionsService from "../services/permissionsService";
import loginService from "../services/loginService";

import ActionTypes from "./types";
import { HOME_PAGE } from "../utils/constants";
import { history, store } from "../store";
// import { buildCameFromURL } from "../utils/util";

export const login = (username, password) => async (dispatch, getState) => {
  store.dispatch({ type: ActionTypes.LOGIN_REQUEST });
  try {
    const baseUrl = window.location.origin;
    const user = await loginService.login(username, password, baseUrl);

    if (user.mfaAuthRequired) {
      if (user.mfaAuthRequired.mfaProvider === "duo") {
        window.location = user.mfaAuthRequired.authUrl;
      }
    } else {
      finishLogin(user);
    }
  } catch (e) {
    store.dispatch({ type: ActionTypes.LOGIN_ERROR, error: e });
  }
};

export const finishLogin = async (user) => {
  permissionsService.clearUserPermissions(); // clear out the previous ones
  setUserAccessToken(user);
  setRefreshAuthTokenTimer(user.ttl);
  // remove it so that user does not tamper with the variable to allow pages. Backend will also verify api calls with permissiosn as well
  // if (user && user.userInfo && user.userInfo.permissions)
  store.dispatch({ type: ActionTypes.LOGIN_SUCCESS, payload: user });

  const siemConfigs = await SIEMService.getAllConfigs();
  if (siemConfigs.configs) {
    siemConfigs.configs.forEach((config, idx) => {
      if (config.url.charAt(config.url.length - 1) === "/") {
        siemConfigs.configs[idx].config.url = config.url.slice(
          0,
          config.url.length - 2
        );
      }
    });
  } // (AP-3640) temp fix for APP link pushes (as APP includes '/' already and do not want double '//')
  store.dispatch({ type: ActionTypes.SIEM_CONFIGS_SET, payload: siemConfigs });

  let queryURLString = window.location.search;
  if (queryURLString.includes("cameFrom=")) {
    history.replace(
      decodeURIComponent(
        queryURLString.substring(queryURLString.lastIndexOf("cameFrom=") + 9)
      )
    );
  } else {
    history.replace(HOME_PAGE);
  }
};
