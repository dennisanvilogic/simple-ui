import React, { PureComponent } from "react";
import { connect } from "react-redux";

import Container from "@material-ui/core/Container";

import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

class HomePage extends PureComponent {
  state = {
    details: null,
    userAccessInfo: null,
  };

  handleClick = (event) => {
    event.preventDefault();
  };

  componentWillMount = (props) => {
    // this.fetchData();
  };

  render() {
    const { details, userAccessInfo } = this.state;
    const { history, dispatch } = this.props;
    return (
      <Container
        style={{
          height: "90vh",
          flexGrow: 1,
          padding: "0px",
          maxWidth: "100%",
          backgroundColor: "black",
        }}
      >
        <Grid container>
          <Button
            variant="primary"
            onClick={() => {
              history.push("/page1");
            }}
          >
            Go To Page 1
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              history.push("/page2");
            }}
          >
            Go To Page 2
          </Button>
        </Grid>
      </Container>
    );
  }
}
const mapStateToProps = (state) => {
  const { isLoading, error, isLoggedIn, user } = state.toJS().application;
  return { isLoading, error, isLoggedIn, user };
};
export default connect(mapStateToProps)(HomePage);
