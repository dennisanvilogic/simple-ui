// import createHistory from "history/createBrowserHistory";

import { createBrowserHistory } from "history";
import { getState } from "./utils/redux-storage";
import configureStore from "./configureStore";

const initialState = getState() || {};
export const history = createBrowserHistory();
export const store = configureStore(initialState, history);
